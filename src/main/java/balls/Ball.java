package balls;

import java.util.Random;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to represent bouncing balls
 * Balls have a size and a color as well as a motion
 * Balls move according to speed and acceleration in both x and y directions.
 * One can set a limit to the movement (like floor ceiling or walls) such that
 * balls will not go outside these limits.
 */
public class Ball {

	/** Color of the ball's surface */
	private Paint color;
	/** The ball's position and speed in x direction. */
	private Motion xMotion ;
	/** The ball's position and speed in y direction. */
	private Motion yMotion;
	/** Number of steps taken */
	private int steps = 0;
	/** The ball's radius  */
	private double radius = 0.0;

	/* Upper limit X */
	private double roofX = 640;
	/* Lower limit X */
	private double floorX = 0 ;
	/* Upper limit Y */
	private double roofY = 480;
	/* Lower limit Y */
	private double floorY = 0 ;
	/* bouncefactor for balls */
	private double bounceFactor = 15.0 ;
	
	/**
	 * Create a new ball with position and velocity (0,0)
	 * 
	 * @param color
	 *               The color of the ball
	 * @param radius
	 *               The radius of the ball
	 */
	public Ball(Paint color, double radius) {
		
		this.color = color;
		this.radius = radius;
		this.steps = 0;
		if (this.radius < 0)
			throw new IllegalArgumentException("Radius should not be negative");

		xMotion = new Motion(0.0, 0.0, 0.0) ;
		yMotion = new Motion(0.0,0.0, 0.0) ;
		this.setLowerLimitX(floorX);
		this.setLowerLimitY(floorY);
		this.setUpperLimitX(roofX);
		this.setUpperLimitY(roofY);
		xMotion.setBounceFactor(bounceFactor);
		yMotion.setBounceFactor(bounceFactor);
		
	}

	/**
	 * @return Current X position of the Ball
	 */
	public double getX() {
		// TODO
		return this.xMotion.getPosition();
	}

	/**.
	 * @return Current Y position of the Ball
	 */
	public double getY() {
		// TODO
		return this.yMotion.getPosition();
	}

	/**
	 * @return The ball's radius
	 */
	public double getRadius() {
		// TODO
		return this.radius;
	}

	/**
	 * @return The ball's width (normally 2x {@link #getRadius()})
	 */
	public double getWidth() {
		// TODO
		return 2*this.radius;
	}
	/**
	 * @return The ball's height (normally 2x {@link #getRadius()})
	 */
	public double getHeight() {
		// TODO
		return 2*this.radius;
	}

	/**
	 * @return Paint/color for the ball
	 */
	public Paint getColor() {
		// TODO
		return this.color;
	}

	/**
	 * Number of steps is used to determine the behavior of the ball
	 * 
	 * @return
	 */
	public int getSteps() {
		return this.steps;
		
	}

	/**
	 * Move ball to a new position.
	 * 
	 * After calling {@link #moveTo(double, double)}, {@link #getX()} will return
	 * {@code newX} and {@link #getY()} will return {@code newY}.
	 * 
	 * @param newX
	 *             New X position
	 * @param newY
	 *             New Y position
	 */
	public void moveTo(double newX, double newY) {
		// TODO
		this.steps ++ ;
		this.xMotion.setPosition(newX);
		this.yMotion.setPosition(newY);
		return ;
	}

	/**
	 * Returns the speed of the ball which is measured in pixels/move
	 * 
	 * @return Current X movement
	 */
	public double getDeltaX() {
		return this.xMotion.getSpeed();
	}

	/**
	 * Returns the speed of the ball which is measured in pixels/move
	 * 
	 * @return Current Y movement
	 */
	public double getDeltaY() {
		return this.yMotion.getSpeed();
	}

	/**
	 * Perform one time step.
	 * 
	 * For each time step, the ball's (xPos,yPos) position should change by
	 * (deltaX,deltaY).
	 */
	public void move() {
		// TODO
		// Hint: examine which methods there are in the class Motion
		// maybe you don't have to do as much as you think.
		this.steps ++ ;

		// for bevegelse i x retning 
		double deltaX = this.getDeltaX();
		//double xPos = this.xMotion.getPosition();
		
		//double yPos = this.yMotion.getPosition();

		double oldSpeed = this.xMotion.getSpeed();
		double oldAccel = this.xMotion.getAcceleration();

		this.xMotion.setSpeed(deltaX);
		this.xMotion.setAcceleration(0.0);	
	
		    this.xMotion.move();
		
		this.xMotion.setSpeed(oldSpeed);
		this.xMotion.setAcceleration(oldAccel);

		// for bevegelse i y retning 
		double deltaY = this.getDeltaY();
		double yPos = this.yMotion.getPosition();

		double oldSpeedY = this.yMotion.getSpeed();
		double oldAccelY = this.yMotion.getAcceleration();
		this.yMotion.setSpeed(deltaY);
		this.yMotion.setAcceleration(0.0);	

		    this.yMotion.move();
	
		this.yMotion.setSpeed(oldSpeedY);
		this.yMotion.setAcceleration(oldAccelY);

		return ;
	}

	/**
	 * This method makes one ball explode into 8 smaller balls with half the radius
	 * The new balls may have different speed and direction
	 * 
	 * @return the new balls after the explosion
	 */
	public Ball[] explode() {
		// TODO
		int numberBalls = 8;
		Ball[] newBalls = new Ball[numberBalls];
		double xPos = this.xMotion.getPosition();
		double yPos = this.yMotion.getPosition();
		double xSpeed = this.xMotion.getSpeed();
		double ySpeed = this.yMotion.getSpeed();
		double xAccel = this.xMotion.getAcceleration();
		double yAccel = this.yMotion.getAcceleration();
		color = this.getColor();
		double radi = this.radius/2 ;

		for ( int i = 0 ; i < numberBalls; i++) {
			newBalls[i]= new Ball(color, radi);
			newBalls[i].xMotion.setPosition(xPos);
			newBalls[i].yMotion.setPosition(yPos);
			newBalls[i].xMotion.setSpeed(xSpeed + 0.0001 );
			newBalls[i].yMotion.setSpeed(ySpeed + 0.0001);
			//newBalls[i].yMotion.setAcceleration(yAcc);
			//newBalls[i].xMotion.setAcceleration(xAcc);
		}
		
		return newBalls;
		
	}

	/**
	 * Acceleration changes the speed of this ball every time move is called.
	 * This method sets the acceleration in both x and y direction to a given value.
	 * This acceleration is then added every time the move method is called
	 * 
	 * @param xAcceleration The extra speed along the x-axis
	 * @param yAcceleration The extra speed along the y-axis
	 */
	public void setAcceleration(double xAcceleration, double yAcceleration) {
		// TODO
		double xAcc = xAcceleration;
		double yAcc = yAcceleration;
		this.xMotion.setAcceleration(xAcc);
		this.yMotion.setAcceleration(yAcc);
		return ;
	}

	/**
	 * This method changes the speed of the ball, this is a one time boost to the
	 * speed
	 * and will only change the speed, not the acceleration of the ball.
	 * 
	 * @param xAcceleration
	 * @param yAcceleration
	 */
	public void accelerate(double xAcceleration, double yAcceleration) {
		// TODO
		double xSpeed = this.xMotion.getSpeed();
		double ySpeed = this.yMotion.getSpeed();
		xSpeed += xAcceleration;
		ySpeed += yAcceleration;
		this.xMotion.setSpeed(xSpeed);
		this.yMotion.setSpeed(ySpeed);
		return ;
	}

	/**
	 * Stops the motion of this ball
	 * Both speed and acceleration will be sat to 0
	 */
	public void halt() {
		// TODO
		this.setAcceleration(0.0, 0.0);
		this.xMotion.setSpeed(0.0);
		this.yMotion.setSpeed(0.0);
		return ;
	}

	/**
	 * Sets the speed of the ball
	 * Note: in BallDemo positive ySpeed is down and negative ySpeed is up
	 * 
	 * @param xSpeed - speed in x direction
	 * @param ySpeed - speed in y direction
	 */
	public void setSpeed(double xSpeed, double ySpeed) {
		this.xMotion.setSpeed(xSpeed);
		this.yMotion.setSpeed(ySpeed);
		return ;
	}

	/**
	 * Sets the lower limit for X values this Ball can have.
	 * If the limit is set the ball will bounce once reaching that limit
	 * 
	 * @param limit
	 */
	public void setLowerLimitX(double limit) {
		this.xMotion.setLowerLimit(limit);
		return ;
	}

	/**
	 * Sets the lower limit for Y values this Ball can have.
	 * If the limit is set the ball will bounce once reaching that limit
	 * 
	 * @param limit
	 */
	public void setLowerLimitY(double limit) {
		this.yMotion.setLowerLimit(limit);
		return ;
	}

	/**
	 * Sets the upper limit for X values this Ball can have.
	 * If the limit is set the ball will bounce once reaching that limit
	 * 
	 * @param limit
	 */
	public void setUpperLimitX(double limit) {
		this.xMotion.setUpperLimit(limit);
		return ;
	}

	/**
	 * Sets the upper limit for Y values this Ball can have.
	 * If the limit is set the ball will bounce once reaching that limit
	 * 
	 * @param limit
	 */
	public void setUpperLimitY(double limit) {
		this.yMotion.setUpperLimit(limit);
		return ;
	}
}
